<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Planning;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Planning controller.
 *
 * @Route("user")
 */
class adminUserController extends Controller
{


    /**
     * Lists all planning entities.
     *
     * @Route("/planning", name="planning_user_index")
     * @Method("GET")
     */
    public function indexAction()
    {

        if (!$this->getUser()->getClasse()) {
            return $this->redirect($this->generateUrl('user_config'));
        }
        $em = $this->getDoctrine()->getManager();

        $planning_ids = [];

        foreach ($this->getUser()->getClasse()->getCour() as $k => $v) {
            if ($v->getPlanning()) {
                $planning_ids[$v->getPlanning()->getId()] = $v->getPlanning()->getId();
            }
        }

        $plannings = $em->getRepository('AppBundle:Planning')->findBy(array(
            'id' => $planning_ids
        ));

        return $this->render('planning/user_index.html.twig', array(
            'plannings' => $plannings,
        ));
    }

    /**
     * Lists all classe entities.
     *
     * @Route("/planning/config/{id}", name="user_planning_show")
     * @Method("GET")
     */
    public function userPlanningShowAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $cours = $em->getRepository('AppBundle:Cour')->findBy(array(
            'planning' => $id
        ));

        $planning = $em->getRepository('AppBundle:Planning')->find($id);
        $matieres = $em->getRepository('AppBundle:Matiere')->findAll();
        $intervenants = $em->getRepository('AppBundle:Intervenant')->findAll();
        $classes = $em->getRepository('AppBundle:Classe')->findAll();
        $salles = $em->getRepository('AppBundle:Salle')->findAll();

        return $this->render('cour/user_planning_show.html.twig', array(
            'cours' => $cours,
            'planning' => $planning,
            'matieres' => $matieres,
            'intervenants' => $intervenants,
            'classes' => $classes,
            'salles' => $salles,
        ));
    }


    /**
     * Lists all classe entities.
     *
     * @Route("/json/all/{id}", name="user_cours_all_json")
     * @Method("GET")
     */
    public function getCoursAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $cours = $em->getRepository('AppBundle:Cour')->findBy(array(
            'planning' => $id
        ));

        $cour = [];

        foreach ($cours as $k => $v) {

            $cour[$k]['id'] = $v->getId();
            $cour[$k]['title'] = $v->getMatiere()->getName();
            $cour[$k]['description'] = $v->getClasse()->getName() . ' : ' . $v->getIntervenant()->getLastName() . ' ' . $v->getIntervenant()->getFirstName();
            $cour[$k]['matiere'] = $v->getMatiere()->getName();
            $cour[$k]['classe'] = $v->getClasse()->getName();
            $cour[$k]['intervenant'] = $v->getIntervenant()->getLastName() . ' ' . $v->getIntervenant()->getFirstName();
            $cour[$k]['salle'] = $v->getSalle()->getName();
            $cour[$k]['start'] = $v->getStartDate()->format('Y-m-d\TH:i:s');
            $cour[$k]['end'] = $v->getEndDate()->format('Y-m-d\TH:i:s');
            $cour[$k]['color'] = $v->getMatiere()->getColor();

        }

        return new JsonResponse($cour);
    }


    /**
     * Lists all planning entities.
     *
     * @Route("/config", name="user_config")
     * @Method("GET")
     */
    public function configAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $classes = $em->getRepository('AppBundle:Classe')->findAll();

        if ($request->getMethod() == 'POST') {

            $user = $em->getRepository('AppBundle:User')->find($this->getUser()->getId());
            $classe = $em->getRepository('AppBundle:Classe')->find($request->request->get('classe'));

            $user->setClasse($classe);
            $em->flush();

            return $this->redirect($this->generateUrl('planning_user_index'));

        }

        return $this->render('planning/user_config.html.twig', array(
            'classes' => $classes
        ));
    }


}
