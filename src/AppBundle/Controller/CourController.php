<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Classe;
use AppBundle\Entity\Cour;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cour controller.
 *
 * @Route("admin/cour")
 */
class CourController extends Controller
{

    /**
     * Lists all classe entities.
     *
     * @Route("/planning/config/{id}", name="planning_config")
     * @Method("GET")
     */
    public function planningConfigAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();

        $cours = $em->getRepository('AppBundle:Cour')->findBy(array(
            'planning'=>$id
        ));
        $planning = $em->getRepository('AppBundle:Planning')->find($id);
        $matieres = $em->getRepository('AppBundle:Matiere')->findAll();
        $intervenants = $em->getRepository('AppBundle:Intervenant')->findAll();
        $classes = $em->getRepository('AppBundle:Classe')->findAll();
        $salles = $em->getRepository('AppBundle:Salle')->findAll();

        return $this->render('cour/index.html.twig', array(
            'cours' => $cours,
            'planning' => $planning,
            'matieres' => $matieres,
            'intervenants' => $intervenants,
            'classes' => $classes,
            'salles' => $salles,
        ));
    }


    /**
     * Lists all classe entities.
     *
     * @Route("/", name="cour_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cours = $em->getRepository('AppBundle:Cour')->findAll();
        $matieres = $em->getRepository('AppBundle:Matiere')->findAll();
        $intervenants = $em->getRepository('AppBundle:Intervenant')->findAll();
        $classes = $em->getRepository('AppBundle:Classe')->findAll();
        $salles = $em->getRepository('AppBundle:Salle')->findAll();

        return $this->render('cour/index.html.twig', array(
            'cours' => $cours,
            'matieres' => $matieres,
            'intervenants' => $intervenants,
            'classes' => $classes,
            'salles' => $salles,
        ));
    }

    /**
     * @Route("/cour/json/find", name="cours_find")
     * @Method("GET")
     */
    public function getCourJsonAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $c = $em->getRepository('AppBundle:Cour')->find($request->query->get('id'));

        $cour = [];

        $cour['id'] = $c->getId();
        $cour['title'] = $c->getMatiere()->getName();
        $cour['description'] = $c->getClasse()->getName().' : '.$c->getIntervenant()->getLastName() . ' ' . $c->getIntervenant()->getFirstName();
        $cour['matiere_id'] = $c->getMatiere()->getId();
        $cour['matiere'] = $c->getMatiere()->getName();
        $cour['classe_id'] = $c->getClasse()->getId();
        $cour['classe'] = $c->getClasse()->getName();
        $cour['intervenant_id'] = $c->getIntervenant()->getId();
        $cour['intervenant'] = $c->getIntervenant()->getLastName() . ' ' . $c->getIntervenant()->getFirstName();
        $cour['salle_id'] = $c->getSalle()->getId();
        $cour['salle'] = $c->getSalle()->getName();
        $cour['start'] = $c->getStartDate()->format('Y-m-d\TH:i:s');
        $cour['end'] = $c->getEndDate()->format('Y-m-d\TH:i:s');
        $cour['color'] = $c->getMatiere()->getColor();

        return new JsonResponse($cour);
    }


    /**
     * Lists all classe entities.
     *
     * @Route("/json/all/{id}", name="cours_all_json")
     * @Method("GET")
     */
    public function getCoursAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();

        $cours = $em->getRepository('AppBundle:Cour')->findBy(array(
            'planning'=>$id
        ));

        $cour = [];

        foreach ($cours as $k => $v) {

            $cour[$k]['id'] = $v->getId();
            $cour[$k]['title'] = $v->getMatiere()->getName();
            $cour[$k]['description'] = $v->getClasse()->getName().' : '.$v->getIntervenant()->getLastName() . ' ' . $v->getIntervenant()->getFirstName();
            $cour[$k]['matiere'] = $v->getMatiere()->getName();
            $cour[$k]['classe'] = $v->getClasse()->getName();
            $cour[$k]['intervenant'] = $v->getIntervenant()->getLastName() . ' ' . $v->getIntervenant()->getFirstName();
            $cour[$k]['salle'] = $v->getSalle()->getName();
            $cour[$k]['start'] = $v->getStartDate()->format('Y-m-d\TH:i:s');
            $cour[$k]['end'] = $v->getEndDate()->format('Y-m-d\TH:i:s');
            $cour[$k]['color'] = $v->getMatiere()->getColor();

        }

        return new JsonResponse($cour);
    }

    /**
     * Lists all classe entities.
     *
     * @Route("/delete", name="delete_cour")
     * @Method("GET")
     */
    public function deleteCourAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');

        $cour = $em->getRepository('AppBundle:Cour')->find($id);

        if(!$cour){
            return new JsonResponse(['deleted'=>false]);
        }

        $em->remove($cour);
        $em->flush();

        return new JsonResponse(['deleted'=>true]);
    }



    /**
     * Lists all classe entities.
     *
     * @Route("/new", name="cour_new")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $classe = $em->getRepository('AppBundle:Classe')->find($request->request->get('classe'));
        $matiere = $em->getRepository('AppBundle:Matiere')->find($request->request->get('matiere'));
        $intervenant = $em->getRepository('AppBundle:Intervenant')->find($request->request->get('intervenant'));
        $salle = $em->getRepository('AppBundle:Salle')->find($request->request->get('salle'));
        $planning = $em->getRepository('AppBundle:Planning')->find($request->request->get('planning'));

        $cour = new Cour();
        $cour->setClasse($classe);
        $cour->setIntervenant($intervenant);
        $cour->setSalle($salle);
        $cour->setMatiere($matiere);
        $cour->setPlanning($planning);
        $cour->setStartDate(\DateTime::createFromFormat('Y-m-d H:i', $request->request->get('start_date')));
        $cour->setEndDate(\DateTime::createFromFormat('Y-m-d H:i', $request->request->get('end_date')));
        $em->persist($cour);

        $em->flush();
        return $this->redirect($this->generateUrl('planning_config',['id'=>$planning->getId()]));

    }


    /**
     * Lists all classe entities.
     *
     * @Route("/edit", name="cour_edit")
     * @Method("POST")
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $cour = $em->getRepository('AppBundle:Cour')->find($request->request->get('id'));
        $planning = $em->getRepository('AppBundle:Planning')->find($request->request->get('planning'));

        $classe = $em->getRepository('AppBundle:Classe')->find($request->request->get('classe'));
        $matiere = $em->getRepository('AppBundle:Matiere')->find($request->request->get('matiere'));
        $intervenant = $em->getRepository('AppBundle:Intervenant')->find($request->request->get('intervenant'));
        $salle = $em->getRepository('AppBundle:Salle')->find($request->request->get('salle'));

        $cour->setClasse($classe);
        $cour->setIntervenant($intervenant);
        $cour->setSalle($salle);
        $cour->setMatiere($matiere);
        $cour->setPlanning($planning);
        $cour->setStartDate(\DateTime::createFromFormat('Y-m-d H:i', $request->request->get('start_date')));
        $cour->setEndDate(\DateTime::createFromFormat('Y-m-d H:i', $request->request->get('end_date')));
        $em->flush();
        return $this->redirect($this->generateUrl('planning_config',['id'=>$planning->getId()]));

    }


    /**
     * Lists all classe entities.
     *
     * @Route("/resize", name="cour_resize")
     * @Method("GET")
     */
    public function resizeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $cour = $em->getRepository('AppBundle:Cour')->find($request->query->get('id'));

        $cour->setStartDate(\DateTime::createFromFormat('Y-m-d H:i', $request->query->get('start')));
        $cour->setEndDate(\DateTime::createFromFormat('Y-m-d H:i', $request->query->get('end')));

        $em->flush();
        return new JsonResponse(true);

    }


}
