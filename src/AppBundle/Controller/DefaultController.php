<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{

    /**
     * @Route("/admin", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $em = $this->get('doctrine.orm.entity_manager');

        $plannings = $em->getRepository('AppBundle:Planning')->findAll();
        $users = $em->getRepository('AppBundle:User')->findAll();
        $classes = $em->getRepository('AppBundle:Classe')->findAll();
        $matieres = $em->getRepository('AppBundle:Matiere')->findAll();
        $intervenants = $em->getRepository('AppBundle:Intervenant')->findAll();
        $filieres = $em->getRepository('AppBundle:Filiere')->findAll();
        $salles = $em->getRepository('AppBundle:Salle')->findAll();

        return $this->render('default/index.html.twig', [
            'plannings'=>count($plannings),
            'users'=>count($users),
            'matieres'=>count($matieres),
            'intervenants'=>count($intervenants),
            'filieres'=>count($filieres),
            'salles'=>count($salles),
            'classes'=>count($classes),
        ]);
    }

    /**
     * @Route("/login-redirect", name="login_redirect")
     */
    public function loginRedirectAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $response = new RedirectResponse($this->get('router')->generate('planning_index'));
        } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $response = new RedirectResponse($this->get('router')->generate('planning_user_index'));
        }
        return $response;
    }


    /**
     * @Route("/demo", name="demo")
     */
    public function demoAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('demo.html.twig', []);
    }

}
