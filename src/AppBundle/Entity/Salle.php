<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SalleRepository")
 */
class Salle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbr_chaise;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbr_table;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbr_ordinateur;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Cour",mappedBy="salle")
     */
    private $cour;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Salle
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nbrChaise
     *
     * @param integer $nbrChaise
     *
     * @return Salle
     */
    public function setNbrChaise($nbrChaise)
    {
        $this->nbr_chaise = $nbrChaise;

        return $this;
    }

    /**
     * Get nbrChaise
     *
     * @return integer
     */
    public function getNbrChaise()
    {
        return $this->nbr_chaise;
    }

    /**
     * Set nbrTable
     *
     * @param integer $nbrTable
     *
     * @return Salle
     */
    public function setNbrTable($nbrTable)
    {
        $this->nbr_table = $nbrTable;

        return $this;
    }

    /**
     * Get nbrTable
     *
     * @return integer
     */
    public function getNbrTable()
    {
        return $this->nbr_table;
    }

    /**
     * Set nbrOrdinateur
     *
     * @param integer $nbrOrdinateur
     *
     * @return Salle
     */
    public function setNbrOrdinateur($nbrOrdinateur)
    {
        $this->nbr_ordinateur = $nbrOrdinateur;

        return $this;
    }

    /**
     * Get nbrOrdinateur
     *
     * @return integer
     */
    public function getNbrOrdinateur()
    {
        return $this->nbr_ordinateur;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Salle
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Salle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cour = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cour
     *
     * @param \AppBundle\Entity\Cour $cour
     *
     * @return Salle
     */
    public function addCour(\AppBundle\Entity\Cour $cour)
    {
        $this->cour[] = $cour;

        return $this;
    }

    /**
     * Remove cour
     *
     * @param \AppBundle\Entity\Cour $cour
     */
    public function removeCour(\AppBundle\Entity\Cour $cour)
    {
        $this->cour->removeElement($cour);
    }

    /**
     * Get cour
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCour()
    {
        return $this->cour;
    }
}
