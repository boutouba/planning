<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MatiereRepository")
 */
class Matiere
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Filiere",inversedBy="matiere")
     * @ORM\JoinColumn(name="filiere_id",onDelete="SET NULL")
     */
    private $filiere;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Cour",mappedBy="matiere")
     */
    private $cour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="color", type="string",length=255)
     */
    private $color;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Matiere
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filiere
     *
     * @param \AppBundle\Entity\Filiere $filiere
     *
     * @return Matiere
     */
    public function setFiliere(\AppBundle\Entity\Filiere $filiere = null)
    {
        $this->filiere = $filiere;

        return $this;
    }

    /**
     * Get filiere
     *
     * @return \AppBundle\Entity\Filiere
     */
    public function getFiliere()
    {
        return $this->filiere;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cour = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cour
     *
     * @param \AppBundle\Entity\Cour $cour
     *
     * @return Matiere
     */
    public function addCour(\AppBundle\Entity\Cour $cour)
    {
        $this->cour[] = $cour;

        return $this;
    }

    /**
     * Remove cour
     *
     * @param \AppBundle\Entity\Cour $cour
     */
    public function removeCour(\AppBundle\Entity\Cour $cour)
    {
        $this->cour->removeElement($cour);
    }

    /**
     * Get cour
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCour()
    {
        return $this->cour;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Matiere
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }


}
