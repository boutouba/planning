-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  jeu. 27 juin 2019 à 00:49
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `university`
--

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

CREATE TABLE `classe` (
  `id` int(11) NOT NULL,
  `filiere_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id`, `filiere_id`, `name`, `created_at`, `updated_at`) VALUES
(1, NULL, 'smpc 1', '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
(2, 1, 'smpc 2', '2014-01-01 00:00:00', '2014-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `cour`
--

CREATE TABLE `cour` (
  `id` int(11) NOT NULL,
  `intervenant_id` int(11) DEFAULT NULL,
  `salle_id` int(11) DEFAULT NULL,
  `classe_id` int(11) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `matiere_id` int(11) DEFAULT NULL,
  `planning_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `cour`
--

INSERT INTO `cour` (`id`, `intervenant_id`, `salle_id`, `classe_id`, `start_date`, `end_date`, `created_at`, `updated_at`, `matiere_id`, `planning_id`) VALUES
(3, 1, 1, 1, '2019-06-23 08:00:00', '2019-06-23 09:00:00', '2019-06-23 10:21:05', '2019-06-23 10:21:05', 1, 1),
(4, 1, 1, 2, '2019-06-23 10:00:00', '2019-06-23 11:00:00', '2019-06-23 10:21:15', '2019-06-23 10:21:15', 1, 1),
(5, 1, 1, 1, '2019-06-24 08:00:00', '2019-06-24 09:00:00', '2019-06-23 10:21:28', '2019-06-23 10:21:28', 1, 1),
(6, 1, 1, 1, '2019-06-24 09:00:00', '2019-06-24 10:00:00', '2019-06-23 10:21:38', '2019-06-23 10:21:38', 1, 1),
(8, 1, 1, 2, '2019-06-18 08:00:00', '2019-06-18 09:00:00', '2019-06-23 10:31:25', '2019-06-23 10:31:25', 1, 1),
(9, 1, 1, 2, '2019-06-19 08:00:00', '2019-06-19 09:00:00', '2019-06-23 10:31:36', '2019-06-23 10:31:36', 1, 1),
(10, 1, 1, 1, '2019-06-20 08:00:00', '2019-06-20 09:00:00', '2019-06-23 10:32:38', '2019-06-23 10:32:38', 1, 1),
(11, 1, 1, 1, '2019-06-21 08:00:00', '2019-06-21 10:00:00', '2019-06-23 10:33:20', '2019-06-23 10:33:20', 1, 1),
(12, 1, 1, 1, '2019-06-22 08:00:00', '2019-06-22 12:00:00', '2019-06-23 10:34:43', '2019-06-23 10:34:43', 1, 1),
(14, 1, 1, 1, '2019-06-19 11:00:00', '2019-06-19 12:30:00', '2019-06-23 22:45:26', '2019-06-23 22:47:44', 1, 1),
(16, 1, 1, 1, '2019-06-26 08:00:00', '2019-06-26 09:00:00', '2019-06-25 00:16:17', '2019-06-25 00:16:17', 1, 1),
(17, 1, 1, 1, '2019-06-26 10:00:00', '2019-06-26 11:00:00', '2019-06-26 21:07:04', '2019-06-26 21:07:04', 2, 1),
(18, 1, 1, 1, '2019-06-28 09:00:00', '2019-06-28 10:00:00', '2019-06-26 22:23:40', '2019-06-26 22:23:40', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `filiere`
--

CREATE TABLE `filiere` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `filiere`
--

INSERT INTO `filiere` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'SVT', '2014-01-01 00:00:00', '2014-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'boutouba', 'boutouba', 'boutoubaismail@gmail.com', 'boutoubaismail@gmail.com', 1, NULL, '$2y$13$I/yAN6LKz.UtThf6OnVknuyNVtVyuCxQv1C0yVmN8h58ucferPnZW', '2019-06-26 21:01:39', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),
(2, 'user', 'user', 'user@user.com', 'user@user.com', 1, NULL, '$2y$13$c0MJFoAJw/LDpK7GGgaPvOFxadb2DoywYbfArBgaFNwFErzQGqFbC', '2019-06-24 20:37:46', NULL, NULL, 'a:0:{}');

-- --------------------------------------------------------

--
-- Structure de la table `intervenant`
--

CREATE TABLE `intervenant` (
  `id` int(11) NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `intervenant`
--

INSERT INTO `intervenant` (`id`, `last_name`, `first_name`, `email`, `phone`, `status`) VALUES
(1, 'boutouba', 'ismail', 'boutoubaismail@gmail.com', '069585858', 1);

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `id` int(11) NOT NULL,
  `filiere_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`id`, `filiere_id`, `name`, `color`) VALUES
(1, 1, 'gestion de projet', '#fe4334'),
(2, 1, 'math', '#be2342');

-- --------------------------------------------------------

--
-- Structure de la table `planning`
--

CREATE TABLE `planning` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `planning`
--

INSERT INTO `planning` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'SVT A1', '2019-06-26 22:08:51', '2019-06-26 22:08:51'),
(2, 'SVT A2', '2019-06-26 22:26:03', '2019-06-26 22:26:03');

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nbr_chaise` int(11) DEFAULT NULL,
  `nbr_table` int(11) DEFAULT NULL,
  `nbr_ordinateur` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id`, `name`, `nbr_chaise`, `nbr_table`, `nbr_ordinateur`, `created_at`, `updated_at`) VALUES
(1, 'A1', 5, 4, 4, '2014-01-01 00:00:00', '2014-01-01 00:00:00');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8F87BF96180AA129` (`filiere_id`);

--
-- Index pour la table `cour`
--
ALTER TABLE `cour`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A71F964FAB9A1716` (`intervenant_id`),
  ADD KEY `IDX_A71F964FDC304035` (`salle_id`),
  ADD KEY `IDX_A71F964F8F5EA509` (`classe_id`),
  ADD KEY `IDX_A71F964FF46CD258` (`matiere_id`),
  ADD KEY `IDX_A71F964F3D865311` (`planning_id`);

--
-- Index pour la table `filiere`
--
ALTER TABLE `filiere`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Index pour la table `intervenant`
--
ALTER TABLE `intervenant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9014574A180AA129` (`filiere_id`);

--
-- Index pour la table `planning`
--
ALTER TABLE `planning`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `classe`
--
ALTER TABLE `classe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `cour`
--
ALTER TABLE `cour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `filiere`
--
ALTER TABLE `filiere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `intervenant`
--
ALTER TABLE `intervenant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `planning`
--
ALTER TABLE `planning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `classe`
--
ALTER TABLE `classe`
  ADD CONSTRAINT `FK_8F87BF96180AA129` FOREIGN KEY (`filiere_id`) REFERENCES `filiere` (`id`);

--
-- Contraintes pour la table `cour`
--
ALTER TABLE `cour`
  ADD CONSTRAINT `FK_A71F964F3D865311` FOREIGN KEY (`planning_id`) REFERENCES `planning` (`id`),
  ADD CONSTRAINT `FK_A71F964F8F5EA509` FOREIGN KEY (`classe_id`) REFERENCES `classe` (`id`),
  ADD CONSTRAINT `FK_A71F964FAB9A1716` FOREIGN KEY (`intervenant_id`) REFERENCES `intervenant` (`id`),
  ADD CONSTRAINT `FK_A71F964FDC304035` FOREIGN KEY (`salle_id`) REFERENCES `salle` (`id`),
  ADD CONSTRAINT `FK_A71F964FF46CD258` FOREIGN KEY (`matiere_id`) REFERENCES `matiere` (`id`);

--
-- Contraintes pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD CONSTRAINT `FK_9014574A180AA129` FOREIGN KEY (`filiere_id`) REFERENCES `filiere` (`id`);
